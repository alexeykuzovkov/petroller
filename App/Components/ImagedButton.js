import React, { Component } from 'react';
import { TouchableOpacity, View, Image} from 'react-native';


export class ImagedButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View>
                    <Image
                        style={{width:35, height:35}}
                        source={this.props.source}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}