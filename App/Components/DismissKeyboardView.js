import React from 'react';
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native';

export const DismissKeyboardView = (props) => {
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            {props.children}
        </TouchableWithoutFeedback>
    );
};