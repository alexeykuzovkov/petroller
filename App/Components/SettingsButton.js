import React, { Component } from 'react';

import {ImagedButton} from './ImagedButton'

export class SettingsButton extends Component {
    render() {
        return (
            <ImagedButton onPress={this.props.onPress} source={require('../Assets/settings.png')} />
        )
    }
}