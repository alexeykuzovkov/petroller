import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export class GradientButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        const style = this.props.style || {};

        const {width, height, borderRadius,elevation,alignItems,justifyContent} = style;
        const buttonStyle = {
            ...style,
            width:width||100,
            height:(height||50),
            borderRadius:borderRadius||8,
            alignItems:alignItems||'center',
            justifyContent:justifyContent||'center',
            elevation:elevation||2,
        };

        const textStyleProp = this.props.textStyle || {}

        const textStyle = {
            ...textStyleProp,
            color:textStyleProp.textColor||'white',
            fontSize:textStyleProp.fontSize||20,
        }

        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <LinearGradient
                    start={{x: 0.0, y: 0.0}} end={{x: 0.0, y: 1.0}}
                    colors={this.props.gradientColors||["#FF0000", "#00FF00"]} style={[buttonStyle]}>
                    <Text style={textStyle} >{this.props.children}</Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}