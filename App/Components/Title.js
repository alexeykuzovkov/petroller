import React, { Component } from 'react';
import { Text } from 'react-native';

export class Title extends Component {

    render() {
        return (
            <Text style={[this.props.style, styles.title]}>{this.props.children}</Text>
        );
    }
}

const styles = {
    title: {
        fontWeight:'700',
        fontSize:30,
        marginBottom:25
    },
}