import React, { Component } from 'react';
import { View, Text, TextInput} from 'react-native';

export const SectionInput = (props) => {
    return (
        <View>
            <Text style={{fontSize:20}}>{props.text}</Text>
            <TextInput
                keyboardType={props.keyboardType}
                placeholder={props.placeholder}
                style={styles.input}
                onChangeText={props.onChangeText}
                value={props.value}
            />
        </View>
    );
};

const styles = {
    input:{height: 50, paddingLeft:10, borderColor: '#c1c1c1', borderRadius:10, borderWidth: 2, marginTop:5}
}