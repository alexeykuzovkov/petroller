import React, { Component } from 'react';
import { View, Text} from 'react-native';

export const Section = (props) => {
  return (
      <View style={[{marginBottom:5, marginTop:5}, props.style]}>
          {props.children}
      </View>
  );
};