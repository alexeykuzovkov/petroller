import React, { Component } from 'react';

import {ImagedButton} from './ImagedButton'

export class ConfirmButton extends Component {
    render() {
        return (
            <ImagedButton onPress={this.props.onPress} source={require('../Assets/check.png')} />
        )
    }
}