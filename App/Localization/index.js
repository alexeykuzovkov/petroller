import LocalizedStrings from 'react-native-localization';

module.exports = new LocalizedStrings({
    "en-US":{
        WelcomeToPetroller:"Welcome to the Petroller",
        PetrollerDesc:"This app will help you calculate cost of your trip according the fuelcost, consumption and duration of your trip",

        Continue:"Continue",

        FuelConsumptionPer:"Fuel consumption per",
        FuelPricePer:"Fuel price per",
        DistanceIn: "Distance in",

        Settings:"Settings",
        Currency:"Currency",
        Unit:"Show distance unit as",
        'Fuel unit':"Show fuel unit as",
        Calculate:"Calculate",

        'US Dollar':"US Dollar",
        'Euro':"Euro",
        'Russian Ruble':"Russian Ruble",
        'KZT':'KZT',
        'UAH':'UAH',
        'BYR':'BYR',

        'liter':'liters',
        'gallon':"gallons",

        'miles':"miles",
        'kilometers':"kilometers",

        'milesA':"miles",
        'kilometersA':"kilometers",

        'milesB':"мiles",
        'kilometersB':"kilometers",

        'literA':'liter',
        'gallonA':"gallon",
    },
    "en":{
        WelcomeToPetroller:"Welcome to the Petroller",
        PetrollerDesc:"This app will help you calculate cost of your trip according the fuelcost, consumption and duration of your trip",

        Continue:"Continue",

        FuelConsumptionPer:"Fuel consumption per",
        FuelPricePer:"Fuel price per",
        DistanceIn: "Distance in",

        Settings:"Settings",
        Currency:"Currency",
        Unit:"Show distance unit as",
        'Fuel unit':"Show fuel unit as",
        Calculate:"Calculate",

        'US Dollar':"US Dollar",
        'Euro':"Euro",
        'Russian Ruble':"Russian Ruble",
        'KZT':'KZT',
        'UAH':'UAH',
        'BYR':'BYR',

        'liter':'liters',
        'gallon':"gallons",

        'miles':"miles",
        'kilometers':"kilometers",

        'milesA':"miles",
        'kilometersA':"kilometers",

        'milesB':"мiles",
        'kilometersB':"kilometers",

        'literA':'liter',
        'gallonA':"gallon",
    },
    "de": {
        WelcomeToPetroller:"Willkommen bei Petroller",
        PetrollerDesc:"Diese App wird Ihnen helfen, die Kosten Ihrer Reise nach Treibstoffkosten, Verbrauch und Dauer Ihrer Reise zu berechnen",

        Continue:"Fortsetzen",

        FuelConsumptionPer:"Kraftstoffverbrauch pro",
        FuelPricePer:"Kraftstoffpreis pro",
        DistanceIn: "Entfernung in",

        Settings:"Einstellungen",
        Currency:"Währung",
        Unit:"Entfernungseinheit anzeigen als",
        'Fuel unit':"Fuel unit anzeigen als",
        Calculate:"Berechnen",

        'US Dollar':"US-Dollar",
        'Euro':"Euro",
        'Russian Ruble':"Russischer Rubel",
        'KZT':'KZT',
        'UAH':'UAH',
        'BYR':'BYR',

        'liter':'liter',
        'gallon':"gallons",

        'miles':"miles",
        'kilometers':"Kilometer",

        'milesA':"miles",
        'kilometersA':"Kilometer",

        'milesB':"мiles",
        'kilometersB':"Kilometer",

        'literA':'liter',
        'gallonA':"gallon",
    },
    "ru": {
        WelcomeToPetroller:"Добро пожаловать в Petroller!",
        PetrollerDesc:"Приложение рассчитывает стоимость поездки по маршруту. Для этого нужно указать расход топлива на 100 километров и его стоимость, а также длительность маршрута.",

        Continue:"Продолжить",

        FuelConsumptionPer:"Потребление топлива на 100",
        FuelPricePer:"Цена топлива за",
        DistanceIn: "Расстояние в",

        Settings:"Настройки",
        Currency:"Валюта",
        Unit:"Расстояние",
        'Fuel unit':"Количество топлива",
        Calculate:"Расчитать",


        'US Dollar':"Доллар США",
        'Euro':"Евро",
        'Russian Ruble':"Российский рубль",
        'KZT':'KZT',
        'UAH':'UAH',
        'BYR':'BYR',


        'liter':'Литры',
        'gallon':"Галлоны",

        'miles':"Мили",
        'kilometers':"Километры",

        'milesA':"миль",
        'kilometersA':"километров",

        'milesB':"милях",
        'kilometersB':"километрах",

        'literA':'литр',
        'gallonA':"галлон",
    }
});