import React  from 'react';

import MainScreen from '../Screens/MainScreen';
import WelcomeScreen from '../Screens/WelcomeScreen';
import SettingsScreen from '../Screens/SettingsScreen';
import SplashScreen from '../Screens/SplashScreen';


import {Router, Scene, Stack} from 'react-native-router-flux'

export const RouterComponent = () => (
    <Router>
        <Stack key="root">
            <Scene key="splash" component={SplashScreen} initial hideNavBar/>
            <Scene key="welcome" component={WelcomeScreen} hideNavBar />
            <Scene key="main" component={MainScreen}  hideNavBar />
            <Scene key="settings" component={SettingsScreen}  hideNavBar />
        </Stack>
    </Router>
);