import {Actions, ActionsConst} from 'react-native-router-flux';
import {FORM_UPDATE, CALCULATE_TRIP_PRICE, OPEN_MAIN_SCREEN, OPEN_SETTINGS_SCREEN, SET_UNIT, SET_IS_LOADED} from './types'
import { Keyboard, AsyncStorage} from 'react-native';

const FIRST_LAUNCH = "first_launch";

const UNIT_SETTING = "unit";
const CURRENCY_SETTING = "currency";
const FUEL_SETTING = "fuel";

export const launchApp = () => {
    return (dispatch) => {

        dispatch(loadSettings());

        AsyncStorage.getItem(FIRST_LAUNCH).then((value) => {
            if (value!==null) {
                dispatch({type:SET_IS_LOADED, payload:true});
                Actions.replace("main");
            }
            else {
                Actions.welcome();
                AsyncStorage.setItem(FIRST_LAUNCH, "true");
            }
        }).done();
    };

};

const loadSettings = () => {
    return (dispatch) => {

        AsyncStorage.getItem(UNIT_SETTING).then((value) => {
            if (value!==null) {
                dispatch(setUnit({prop:'unit', value}));
            }
        }).done();

        AsyncStorage.getItem(CURRENCY_SETTING).then((value) => {
            if (value!==null) {
                dispatch(setUnit({prop:'currency', value}));
            }
        }).done();

        AsyncStorage.getItem(FUEL_SETTING).then((value) => {
            if (value!==null) {
                dispatch(setUnit({prop:'fuel', value}));
            }
        }).done();

    };
};

const saveSetting = ({prop, value}) => {
    AsyncStorage.setItem(prop, value);
};

export const openMainScreen = () => {
    Actions.main();
    return {
        type:OPEN_MAIN_SCREEN
    }
};


export const openSettingsScreen = () => {
    Actions.settings();
    return {
        type:OPEN_SETTINGS_SCREEN
    }
};

export const closeSettingsScreen = () => {
  Actions.main();
    return {
        type:'s'
    }
};

export const setUnit = ({prop, value}) => {
    saveSetting({prop,value});
    return {
        type:SET_UNIT,
        payload: {prop, value}
    }
};




export const calculateTripPrice = ({consumption, price, distance}) => {

    Keyboard.dismiss();

    let l_cons = isNaN(consumption.replace(',','.')) || consumption==="" ? 0 : parseFloat(consumption);
    let l_price = isNaN(price.replace(',','.')) || price==="" ? 0 : parseFloat(price);
    let l_dist = isNaN(distance.replace(',','.')) || distance==="" ? 0 : parseFloat(distance);

    const tripPrice = l_price*((l_cons/100)*l_dist);


    return {
        type:CALCULATE_TRIP_PRICE,
        payload:tripPrice.toFixed(1)
    }
};


export const formUpdate = ({prop,value}) => {
    return {
        type:FORM_UPDATE,
        payload: {prop, value}
    }
}