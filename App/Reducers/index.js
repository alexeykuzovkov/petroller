import {combineReducers} from 'redux';

import MainFormReducer from './MainFormReducer';
import TripPriceReducer from './TripPriceReducer';
import SettingsReducer from './SettingsReducer';
import AppReducer from './AppReducer';

export default combineReducers({
    app:AppReducer,
    mainForm: MainFormReducer,
    trip:TripPriceReducer,
    settings:SettingsReducer
});