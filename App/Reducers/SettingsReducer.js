import { SET_UNIT } from '../Actions/types'

const INITIAL_STATE = {
    unit:'miles',
    currency:'US Dollar',
    fuel:'gallon',

    units: [
        'miles',
        'kilometers'
    ],

    fuelUnits: [
        'liter',
        'gallon'
    ],

    currencies: [
        'US Dollar',
        'Euro',
        'Russian Ruble',
        'KZT',
        'UAH',
        'BYR'
    ],

    currenciesSymbols: {
        'US Dollar': '$',
        'Euro': '€',
        'Russian Ruble': '₽',
        'KZT': 'KZT',
        'UAH': '₴',
        'BYR': 'Br'
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_UNIT:
            return { ...state, [action.payload.prop]:action.payload.value };
        default:
            return { ...state };
    }
}