import { FORM_UPDATE } from '../Actions/types'

const INITIAL_STATE = {
    consumption:"0",
    price:"0",
    distance:"0"
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FORM_UPDATE:
            return {...state, [action.payload.prop]: action.payload.value};
        default:
            return {...state};
    }
}