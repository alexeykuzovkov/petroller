import { SET_IS_LOADED } from '../Actions/types'

const INITIAL_STATE = {
    isLoaded:false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_IS_LOADED:
            return {...state, isLoaded:action.payload};
        default:
            return {...state};
    }
}