import { CALCULATE_TRIP_PRICE } from '../Actions/types'

const INITIAL_STATE = {
    trip_price:null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CALCULATE_TRIP_PRICE:
            return {...state, trip_price:action.payload};
        default:
            return {...state};
    }
}