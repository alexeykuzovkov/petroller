import React, { Component } from 'react';
import { Text, Image, View, SectionList, TouchableOpacity, Button} from 'react-native';

import { Title, ConfirmButton} from '../Components'

import {connect} from 'react-redux'

import {
    setUnit,
    closeSettingsScreen
} from '../Actions'

const Locale = require('../Localization');

class SettingsScreen extends React.Component {
    capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    selectItem(title, index) {
        console.log(title+" "+index);
        switch (title) {
            case 'Currency':
                this.props.setUnit({prop:'currency', value:this.props.currencies[index]});
                break;
            case 'Unit':
                this.props.setUnit({prop:'unit', value:this.props.units[index]});
                break;
            case 'Fuel unit':
                this.props.setUnit({prop:'fuel', value:this.props.fuelUnits[index]});
                break;
        }


    }

    rowItem({item, index, section}) {
        let checkmark = '';


        switch (section.title) {
            case 'Currency':
                checkmark = item===this.props.currency? "✓":"";
                break;
            case 'Unit':
                checkmark = item===this.props.unit?"✓":"";
                break;
            case 'Fuel unit':
                checkmark = item===this.props.fuel?"✓":"";
                break
        }
        return (
            <TouchableOpacity onPress={()=>this.selectItem(section.title, index)}>
                <View style={styles.row}>
                    <Text style={styles.rowText} key={index}>{Locale[item]}</Text>
                    <Text style={styles.checkmark}>{checkmark}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Title style={{flex:1}}>{Locale.Settings}</Title>
                    <View style={styles.confirmButtonContainer}>
                        <ConfirmButton onPress={()=>this.props.closeSettingsScreen()} />
                    </View>
                </View>

                <View style={{flex:1}}>
                    <SectionList
                        renderItem={(props) => this.rowItem(props) }
                        renderSectionHeader={({section: {title}}) => (
                            <View style={{ backgroundColor:'white', padding:0}}>
                                <Text style={styles.sectionTitle}>{Locale[title]}</Text>
                            </View>
                        )}
                        sections={[
                            {title: 'Currency', data: this.props.currencies},
                            {title: 'Unit', data: this.props.units},
                            {title: 'Fuel unit', data: this.props.fuelUnits},
                        ]}
                        keyExtractor={(item, index) => item + index}
                    />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        paddingTop:40,
        paddingLeft:10,
        backgroundColor:'white',
        flex:1
    },
    header: {
        flexDirection:'row'
    },
    sectionTitle: {
        fontWeight:'bold', fontSize:20, marginBottom:15, marginTop:15
    },

    confirmButtonContainer: {
        alignItems:'flex-end',
        backgrounColor:'red',
        width:50,
        marginRight:10
    },

    row: {
        height:50,
        borderBottomWidth:1,
        borderColor:'#c1c1c1',
        alignItems:'center',
        paddingRight:-20,
        flexDirection:'row'
    },

    rowText: {
        flex:1,
        fontSize:20
    },

    checkmark: {
        fontSize:20,
        width:40
    }


};

const mapStateToProps = state => {
    const {
        unit,
        currency,
        units,
        fuel,
        fuelUnits,
        currencies
    } = state.settings;

    return {
        unit,
        currency,
        units,
        fuel,
        currencies,
        fuelUnits
    };
};

const actions = {
    setUnit,
    closeSettingsScreen
};

export default connect(mapStateToProps, actions )(SettingsScreen)