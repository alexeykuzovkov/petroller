import React, { Component } from 'react';
import { Text, Image, View} from 'react-native';

import {
    openMainScreen
} from '../Actions';

import {GradientButton, Title} from '../Components'

import {connect} from 'react-redux'

const Locale = require('../Localization');

class WelcomeScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imageWrapper}>
                    <Image
                        style={styles.iconImage}
                        source={require('../Assets/rounded_icon.png')}
                    />
                </View>
                <View style={styles.textWrapper}>
                    <Title>{Locale.WelcomeToPetroller}</Title>
                    <Text style={styles.subtitle}>{Locale.PetrollerDesc}</Text>
                </View>
                <View style={styles.buttonWrapper}>
                    <GradientButton
                        onPress={this.props.openMainScreen}
                        style={styles.buttonStyle}
                        gradientColors={["#5a5a5a", "#030303"]}>
                        {Locale.Continue}
                        </GradientButton>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flexDirection:'column',
        flex:1,
        backgroundColor:'white'
    },
    imageWrapper: {
        flex:0.3,
        alignItems:'center',
        justifyContent:'center'
    },
    iconImage: {
        borderRadius:24,
        width:100,
        height:100
    },


    textWrapper: {
        marginLeft:25,
        marginRight:25,
        flex:0.5
    },

    subtitle: {
        fontFamily:'system font',
        fontSize:25
    },

    buttonWrapper:{
        flex:0.2,
        alignItems:'center',
        justifyContent:'center'
    },
    buttonStyle:{
        width:200,
        borderRadius:10
    },
};

const mapStateToProps = state => {
    return {
    };
};

const actions = {
    openMainScreen
};

export default connect(mapStateToProps, actions )(WelcomeScreen)