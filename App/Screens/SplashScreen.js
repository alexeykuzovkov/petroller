import React, { Component } from 'react';
import { View } from 'react-native';


import {
    launchApp
} from '../Actions';

import {connect} from 'react-redux'

class SplashScreen extends React.Component {
    componentDidMount() {
        this.props.launchApp();
    }

    render() {
        return (
            <View style={styles.container} />
        );
    }
}

const styles = {
    container: {
        paddingTop:30,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:'white',
        flex:1
    }
};


const actions = {
    launchApp
};

export default connect(null, actions )(SplashScreen)