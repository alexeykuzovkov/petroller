import React, { Component } from 'react';
import { Text, View, Image, Button} from 'react-native';

import {GradientButton, SettingsButton, Title, Section, SectionInput, DismissKeyboardView} from '../Components'

import {
    formUpdate,
    calculateTripPrice,
    openSettingsScreen
} from '../Actions';

import {connect} from 'react-redux'

const Locale = require('../Localization');

class MainScreen extends React.Component {

    price_view() {
        const { currenciesSymbols, currency, trip_price} = this.props;
        const symbol = currenciesSymbols[currency];

        const price = trip_price!==null ? trip_price+symbol : '';

        return (
            <View style={styles.priceWrapper}>
                <Text style={styles.price}>{price}</Text>
            </View>
        );
    }
    render() {
        return (
            <DismissKeyboardView>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image
                            style={styles.iconImage}
                            source={require('../Assets/rounded_icon.png')}
                        />
                        <Title style={{flex:1}}>Petroller</Title>
                        <View style={styles.settingsButtonContainer}>
                            <SettingsButton onPress={()=>this.props.openSettingsScreen()} />
                        </View>
                    </View>
                    <View style={styles.content}>
                        <Section>
                            <SectionInput
                                keyboardType="numeric"
                                text={`${Locale.FuelConsumptionPer} ${Locale[this.props.unit+"A"]}`}
                                onChangeText={(value)=>this.props.formUpdate({prop:'consumption',value:value})}
                                value={this.props.consumption}
                            />
                        </Section>
                        <Section>
                            <SectionInput
                                keyboardType="numeric"
                                text={`${Locale.FuelPricePer} ${Locale[this.props.fuel+"A"]}`}
                                onChangeText={(value)=>this.props.formUpdate({prop:'price',value:value})}
                                value={this.props.price}
                            />
                        </Section>
                        <Section>
                            <SectionInput
                                keyboardType="numeric"
                                text={`${Locale.DistanceIn} ${Locale[this.props.unit+"B"]}`}
                                onChangeText={(value)=>this.props.formUpdate({prop:'distance',value:value})}
                                value={this.props.distance}
                            />
                        </Section>

                        <Section style={{marginTop:15}}>
                            <GradientButton
                                onPress={()=> {
                                    this.props.calculateTripPrice(this.props)
                                }}
                                style={styles.buttonStyle}
                                gradientColors={["#5a5a5a", "#030303"]}>
                                {Locale.Calculate}
                            </GradientButton>
                        </Section>
                    </View>
                    {this.price_view()}
                </View>
            </DismissKeyboardView>
        );
    }
}

const styles = {
    container: {
        paddingTop:30,
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:'white',
        flex:1
    },

    header: {
        flexDirection:'row'
    },

    iconImage: {
        width:40,
        height:40,
        borderRadius:10,
        marginRight:10
    },

    settingsButtonContainer: {
        alignItems:'flex-end',
        backgrounColor:'red',
        width:50
    },

    content: {
        flex:0.6
    },

    buttonStyle:{
        width:'100%',
        borderRadius:10
    },

    priceWrapper: {
        alignItems:'center',
        justifyContent:'center',
        flex:0.3
    },

    price: {
        fontSize:60
    }
};

const mapStateToProps = state => {
    const {
        consumption,
        price,
        distance
    } = state.mainForm;

    const {
        unit,
        fuel,
        currenciesSymbols,
        currency
    } = state.settings;

    return {
        consumption,
        price,
        distance,
        unit,
        fuel,
        currenciesSymbols,
        currency,
        trip_price:state.trip.trip_price
    };
};

const actions = {
    formUpdate,
    calculateTripPrice,
    openSettingsScreen
};

export default connect(mapStateToProps, actions )(MainScreen)